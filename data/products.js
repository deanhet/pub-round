// CREATE TABLE products (
//     id SERIAL NOT NULL PRIMARY KEY,
//     name text NOT NULL,
//     image_url TEXT NULL
// )

module.exports = [
  {
    id: 1,
    name: 'Zeitgeist',
    image_url: 'https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_94573.jpg'
  },
  {
    id: 2,
    name: 'Joker',
    image_url: 'http://assets.williamsbrosbrew.com/brews/joker-ipa/_brew_portrait_large/WB_Bottle_500ml_Joker-IPA.jpg?mtime=20160201121726'
  },
    {
    id: 3,
    name: 'Punk IPA',
    image_url: 'https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_135361.jpg'
  }
  ,  {
    id: 4,
    name: 'Edinburgh Gold',
    image_url: 'https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_55716.jpg'
  },
  {
    id: 5,
    name: 'Ossian',
    image_url: 'http://www.inveralmond-brewery.co.uk/userfiles/image/image.php/ossian-bottleshot-feb14.png?width=320&image=/userfiles/image/ossian-bottleshot-feb14.png'
  }
];
