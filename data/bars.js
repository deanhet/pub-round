// CREATE TABLE bars (
//     id SERIAL NOT NULL PRIMARY KEY,
//     name TEXT NOT NULL,
//     lat DECIMAL(18,4) NULL,
//     long DECIMAL(18,4) NULL,
//     image_url TEXT NULL
// )

module.exports = [
  {
    id: 1,
    name: 'The Standing Order',
    lat: 55.9529276,
    long: -3.2017751,
    image_url: 'http://3.bp.blogspot.com/_6Sf8K1q8RCk/S8DFgmL2wNI/AAAAAAAAD3o/vsePkjS4uXQ/s1600/weatherspoons+front+door.jpg'
  },
  {
    id: 2,
    name: 'The Red Squirrel',
    lat: 55.9484014,
    long: -3.2084313,
    image_url: 'https://media-cdn.tripadvisor.com/media/photo-s/02/2c/37/d5/red-squirrel.jpg'
  },
  {
    id: 3,
    name: 'The Tron',
    lat: 55.9499471,
    long: -3.1878261,
    image_url: 'http://www.spottedbylocals.com/edinburgh/files/2016/11/Tron-Pub-Edinburgh-by-Gwilym-Sims-Williams.jpg'
  },
  {
    id: 4,
    name: 'The Caley Picture House',
    lat: 55.9496545,
    long: -3.2051211,
    image_url: 'http://photos.cinematreasures.org/production/photos/159473/1456080167/large.jpg?1456080167'
  }
];
