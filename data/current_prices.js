// CREATE TABLE current_prices (
//     bar_id INT NOT NULL,
//     product_id INT NOT NULL,
//     current_price DECIMAL(12,2) NOT NULL
// )

module.exports = [
  {
    barId: 1,
    productId: 1,
    currentPrice: 3.50
  },
  {
    barId: 1,
    productId: 2,
    currentPrice: 4.00
  },
  {
    barId: 1,
    productId: 3,
    currentPrice: 4.15
  },
  {
    barId: 1,
    productId: 4,
    currentPrice: 4.25
  },
  {
    barId: 1,
    productId: 5,
    currentPrice: 1.50
  },
  //////
  {
    barId: 2,
    productId: 1,
    currentPrice: 1.99
  },
  {
    barId: 2,
    productId: 3,
    currentPrice: 3.70
  },
  {
    barId: 2,
    productId: 4,
    currentPrice: 4.20
  },
  ///////
  {
    barId: 3,
    productId: 3,
    currentPrice: 3.80
  },
  {
    barId: 3,
    productId: 2,
    currentPrice: 3.00
  },
  {
    barId: 3,
    productId: 5,
    currentPrice: 2.45
  },
  ////////
  {
    barId: 4,
    productId: 1,
    currentPrice: 1.90
  },
  {
    barId: 4,
    productId: 2,
    currentPrice: 4.10
  },
  {
    barId: 4,
    productId: 3,
    currentPrice: 3.25
  },
  {
    barId: 4,
    productId: 4,
    currentPrice: 3.20
  },
  {
    barId: 4,
    productId: 5,
    currentPrice: 2.50
  }
];
