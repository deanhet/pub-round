import React from 'react';
import { Grid, Row, Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import './App.css';

export default class App extends React.Component {
  render() {
    return (
      <Grid fluid>
        <Row>
          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <LinkContainer to="/">
                  <span>Pub-round</span>
                </LinkContainer>
              </Navbar.Brand>
              <Nav>
                <LinkContainer to="/" className="col-xs-12">
                  <NavItem eventkey={1}>Bars</NavItem>
                </LinkContainer>
                {/* Future functionality
                  <NavItem eventkey={2} href="#">Previous Rounds</NavItem>
                */}
              </Nav>
            </Navbar.Header>
          </Navbar>
        </Row>
        <Grid>
          {this.props.children}
        </Grid>
      </Grid>
    );
  }
}
