import React from 'react';
import { Col } from 'react-bootstrap';

export default class RoundSummary extends React.Component{
  render(){
    const { totalPrice, onClick } = this.props;
    return(
      <div className='notification-bar'>
        <Col xs={12} onClick={onClick}>
          <Col xs={6}>
            <h3>To the bar!</h3>
          </Col>
          <Col className="text-right" xs={6}>
            <h3>£{totalPrice}</h3>
          </Col>
        </Col>
      </div>
    )
  }
}
