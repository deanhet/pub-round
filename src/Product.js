import React from 'react';
import { Panel, Grid, Col, Row } from 'react-bootstrap';
import Icon from 'react-fontawesome';

export default class Product extends React.Component{
  render(){
    const { product, handleOnAddProduct, handleOnRemoveProduct, active, checkbox, onToggleCheck, checked } = this.props;
    return(
      <Panel onClick={checkbox ? onToggleCheck : null}>
        <Grid>
          <Row>
            <Col xs={2}>
              <img style={{ maxHeight: 150, width: 50 }} alt="product" className="img-responsive" src={product.image_url} />
            </Col>
            <Col xs={3}>
              <h4>{product.name}</h4><br/>
              <h5>£{product.currentPrice || product.actualPrice}</h5>
            </Col>
            { checkbox ?
            <div>
              <Col xs={5} className="text-center">
                <Icon style={{lineHeight: '150px', fontSize:100}} name={checked ? "check-square-o" : "square-o"} size="2x" />
              </Col>
            </div>
            :
            <div className="col-sm-6 col-xs-12">
              <Col xs={3} className="text-center" onClick={() => handleOnAddProduct(product)}>
                <span style={{ fontSize: 100 }}>+</span>
              </Col>
              <Col className="text-center" xs={6}>
                <h1 style={{lineHeight: 3}}>{active.length}</h1>
              </Col>
              <Col xs={3} onClick={() => handleOnRemoveProduct(product)}>
                <span style={{ fontSize: 100 }}>-</span>
              </Col>
            </div>
            }
          </Row>
        </Grid>
      </Panel>
    );
  }
}
