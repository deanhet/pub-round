export function combineProductInfo(products, combineWith){
  return products.reduce((combinedListings, product) => {
    combineWith.forEach((barProduct) => {
      if(product.id === barProduct.productId){
        combinedListings.push({...product, ...barProduct});
      }
    });
    return combinedListings;
  }, []);
}
