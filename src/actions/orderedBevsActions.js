import { createAction } from 'redux-actions';

export const ADD_SELECTED_PRODUCTS = 'ADD_SELECTED_PRODUCTS';
export const createOrderedBevsAction = createAction(ADD_SELECTED_PRODUCTS);
