import { createAction } from 'redux-actions';
import { createOrderedBevsAction } from './orderedBevsActions';

export const CREATE_ROUND = 'CREATE_ROUND';
const createRoundAction = createAction(CREATE_ROUND);

export function createRoundWithProductsAction(barId, nextId, products){
  return (dispatch) => {
    const structureProducts = products.map((product, index) => {
      return {
        // Should use a UUID, doesn't really matter for this
        id: Math.floor(Math.random() * 1000),
        roundId: nextId,
        productId: product.productId,
        actualPrice: product.currentPrice
      }
    });
    dispatch(createOrderedBevsAction(structureProducts))
    dispatch(createRoundAction({barId, nextId}));
  }
}
