import React from 'react';
import { Panel, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router';
import Icon from 'react-fontawesome';

export default class BarItem extends React.Component{
  render(){
    const { bar } = this.props;
    const mapsLink = `https://maps.google.com/?q=${bar.lat},${bar.long}`
    return(
      <Row>
        <Col xs={12}>
          <Panel>
            <Link to={`/bar/${bar.id}`}>
              <Col xs={10}>
                <Col xs={2}>
                  <img style={{ maxHeight: 200, width: 100 }} alt="product" className="img-responsive" src={bar.image_url} />
                </Col>
                <h4>{bar.name}</h4>
              </Col>
            </Link>
            <Col xs={2}>
            <a href={mapsLink} target="_blank">
              <Icon style={{ fontSize: 40 }} name="map-marker" />
            </a>
            </Col>
          </Panel>
        </Col>
      </Row>
    );
  }
}
