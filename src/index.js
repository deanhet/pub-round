import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import createLogger from 'redux-logger';
import pubRound from './reducers/index';
import ReactDOM from 'react-dom';
import App from './App';
import Bars from './Bars';
import Bar from './Bar';
import Round from './Round';
import './index.css';

const logger = createLogger();
const store = createStore(pubRound, applyMiddleware(thunk, logger));
const history = syncHistoryWithStore(hashHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Bars} />
        <Route path="/bar/:barId" component={Bar} />
        <Route path="/round/:roundId/:roundProgress" component={Round} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
