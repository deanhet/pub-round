import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Row, Col, PageHeader } from 'react-bootstrap';
import Product from './Product';
import { combineProductInfo } from './actions/productActions';

export class Round extends React.Component{
  constructor(props){
    super(props);
    this.toggleCheck = this.toggleCheck.bind(this);
    this.state = {
      checkedItems: []
    }
  }
  toggleCheck(index){
    if(this.state.checkedItems.includes(index)){
      //remove
      const newIndexes = this.state.checkedItems.filter((item) => item !== index);
      this.setState({
        checkedItems: newIndexes
      })
    } else {
      //add
      this.setState({
        checkedItems: this.state.checkedItems.concat([index])
      });
    }
  }
  render(){
    const { products, orderedBeverages, params} = this.props;
    const roundBevs = orderedBeverages.filter((beverage) => beverage.roundId === Number(params.roundId))
    const productsWithInfo = combineProductInfo(products, roundBevs);
    return(
      <Row>
        <Col xs={12}>
          <PageHeader><small>Check off your drinks as they come in</small></PageHeader>
        </Col>
        <Col xs={12}>
          {productsWithInfo.map((product, index) =>
            <Product
              key={product.id}
              product={product}
              checkbox={params.roundProgress === 'new'}
              onToggleCheck={() => this.toggleCheck(index)}
              checked={this.state.checkedItems.includes(index)}
            />
          )}
          {this.state.checkedItems.length === productsWithInfo.length &&
            <Link to="/">
              <div className="notification-bar">
                <Col xs={12} className="text-right">
                  <h3>Done!</h3>
                </Col>
              </div>
            </Link>
          }
        </Col>
      </Row>
    );
  }
}

function select(state){
  return {
    products: state.products.items,
    orderedBeverages: state.orderedBeverages.items
  }
}

export default connect(select)(Round);
