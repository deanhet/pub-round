import React from 'react';
import { connect } from 'react-redux';
import { PageHeader } from 'react-bootstrap';
import { Row, Col, Well } from 'react-bootstrap';

import Product from './Product';
import RoundSummary from './RoundSummary';
import { createRoundWithProductsAction } from './actions/roundActions';
import { combineProductInfo } from './actions/productActions';

class Bar extends React.Component{
  constructor(props){
    super(props);
    this.findCurrentBar = this.findCurrentBar.bind(this);
    this.findCurrentBarProducts = this.findCurrentBarProducts.bind(this);
    this.createRound = this.createRound.bind(this);
    this.addroduct = this.addroduct.bind(this);
    this.removeProduct = this.removeProduct.bind(this);
    this.state = {
      chosenItems: []
    }
  }
  createRound(){
    const nextRoundId = this.props.rounds.length + 1;
    this.props.dispatch(createRoundWithProductsAction(Number(this.props.params.barId), nextRoundId, this.state.chosenItems));
    this.props.router.push(`/round/${nextRoundId}/new`);
  }
  findCurrentBar(){
    return this.props.bars.find((bar) => bar.id === Number(this.props.params.barId));
  }
  addroduct(toggledProduct){
    this.setState({
      chosenItems: this.state.chosenItems.concat([toggledProduct])
    });
  }
  removeProduct(toggledProduct){
    const productIndex = this.state.chosenItems.findIndex((items) => items.productId === toggledProduct.productId);
    const removedProduct = this.state.chosenItems.slice(productIndex + 1);
    this.setState({ chosenItems: removedProduct });
  }
  findCurrentBarProducts(){
    const { products, currentPrices, params } = this.props;
    const barProducts = currentPrices.filter((currentPrice) => currentPrice.barId === Number(params.barId))
    return combineProductInfo(products, barProducts);
  }
  render(){
    const currentBar = this.findCurrentBar();
    const totalPrice = this.state.chosenItems.reduce((totalPrice, product) => totalPrice += product.currentPrice, 0);
    return(
      <Row>
        <Col xs={12}>
          <PageHeader>{currentBar.name} <small>Add products to your round</small></PageHeader>
        </Col>
          <Well>
            <h2>Products</h2>
            {this.findCurrentBarProducts().map((product) => {
              const numberSelected = this.state.chosenItems.filter((chosenItem) => chosenItem.productId === product.id);
              return (
              <Product
                active={numberSelected}
                key={product.id}
                product={product}
                handleOnAddProduct={this.addroduct}
                handleOnRemoveProduct={this.removeProduct}
              />);
            }
            )}
          </Well>
          {totalPrice > 0 && <RoundSummary
            totalPrice={totalPrice.toFixed(2)}
            onClick={this.createRound}
          />}
      </Row>
    );
  }
}

function select(state){
 return {
    bars: state.bars.items,
    products: state.products.items,
    currentPrices: state.currentPrices.items,
    orderedBeverages: state.orderedBeverages.items,
    rounds: state.rounds.items
 }
}

export default connect(select)(Bar);
