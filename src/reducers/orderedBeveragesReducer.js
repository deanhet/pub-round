import { ADD_SELECTED_PRODUCTS } from '../actions/orderedBevsActions';
const orderedBeverageData = require('../../data/ordered_beverages');
export default function orderedBeverages(state = {
  items: orderedBeverageData
}, action){
  switch(action.type){
    case ADD_SELECTED_PRODUCTS: {
      const items = state.items.concat(action.payload)
      return { ...state, items}
    }
    default:
      return state;
  }
};
