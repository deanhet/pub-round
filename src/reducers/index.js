import { combineReducers } from 'redux';

import { routerReducer } from 'react-router-redux';

import bars from './barsReducer';
import currentPrices from './currentPricesReducer';
import orderedBeverages from './orderedBeveragesReducer';
import products from './productsReducer';
import rounds from './roundsReducer';

export default combineReducers({
  bars,
  currentPrices,
  orderedBeverages,
  products,
  rounds,
  routing: routerReducer
});
