const roundData = require('../../data/rounds');
import { CREATE_ROUND } from '../actions/roundActions';

export default function rounds(state = {
  items: roundData
}, action){
  switch(action.type){
    case CREATE_ROUND: {
      const { nextId, barId } = action.payload;
      const items = state.items.concat([{ id: nextId, barId, orderedAt: Date.now()}])
      return { ...state, items}
    }
    default:
      return state;
  }
};
