const currentPriceData = require('../../data/current_prices');
export default function currentPrices(state = {
  items: currentPriceData
}, action){
  switch(action.type){
    default:
      return state;
  }
};
