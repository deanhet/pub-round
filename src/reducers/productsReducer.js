const productData = require('../../data/products');
export default function products(state = {
  items: productData
}, action){
  switch(action.type){
    default:
      return state;
  }
};
