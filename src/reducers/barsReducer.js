const barData = require('../../data/bars');
export default function bars(state = {
  items: barData
}, action){
  switch(action.type){
    default:
      return state;
  }
};
