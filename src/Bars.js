import React from 'react';
import BarItem from './BarItem';
import { connect } from 'react-redux';
import { Row, Col, Well, PageHeader } from 'react-bootstrap';

class Bars extends React.Component{
  render(){
    const { bars } = this.props;
    return(
      <Row>
        <Col xs={12}>
          <PageHeader>Bars <small>Select your location</small></PageHeader>
          <Well>
            {bars.map((bar) =>
              <BarItem
                key={bar.id}
                bar={bar}
              />
            )}
          </Well>
        </Col>
      </Row>
    )
  }
}

function select(state){
  return{
    bars: state.bars.items
  }
}

export default connect(select)(Bars);
