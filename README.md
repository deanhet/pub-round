To get this project up and running, clone it then:

* `npm install`
* `npm start`

Live demo available [here](http://deanhet.com/pub-round/index.html#/).

Job done!